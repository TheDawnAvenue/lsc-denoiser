﻿ Param(
    [Parameter(Mandatory=$True)]
    [string]$InputDir,

    [Parameter(Mandatory=$True)]
    [string]$Model,

    [Parameter(Mandatory=$True)]
    [string]$OutputDir 
 )


 Get-ChildItem "$InputDir" | ForEach-Object {
    Write-Host $_.FullName
    .\denoise.py "$Model" $_.FullName $(Join-Path $OutputDir $_.Name)
 }