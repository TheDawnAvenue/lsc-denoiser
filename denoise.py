import sys

import torch
from PIL import Image
import conv_filter
import auto_filter
import autoencoder_28
import autoencoder_56
import numpy as np
from torchvision import transforms

TILE_SIZE = 236


def get_device():
    if torch.cuda.is_available():
        device = 'cuda:0'
    else:
        device = 'cpu'
    return device


def load_net(net_path):
    net = conv_filter.DenoiserModel().float()
    net.load_state_dict(torch.load(net_path))
    net = net.to(get_device())
    return net


def denoise_img(net, source, target):
    source_img = Image.open(source).convert('RGB')
    target_img = Image.new('RGB', source_img.size)
    device = get_device()
    to_tensor = transforms.ToTensor()
    to_pil = transforms.ToPILImage()
    net.eval()

    x = 0
    y = 0

    while y < source_img.height:
        while x < source_img.width:
            tile = source_img.crop((x, y, x + TILE_SIZE, y + TILE_SIZE))

            with torch.no_grad():
                input_img = to_tensor(tile).unsqueeze(0)
                input_img = np.clip(input_img, 0., 1.)
                input_img = input_img.to(device)

                output = net(input_img.float())
                tile = to_pil(output.cpu().data.squeeze(0))

            target_img.paste(tile, (x, y))

            x += TILE_SIZE
        y += TILE_SIZE
        x = 0

    target_img.save(target)


def parse_args():
    if len(sys.argv) != 4:
        return "Unexpected argument list. Expected 3 arguments.", None, None, None

    return None, sys.argv[1], sys.argv[2], sys.argv[3]


if __name__ == '__main__':
    err, net_path, source, target = parse_args()
    if err is not None:
        print(err)
    else:
        net = load_net(net_path)
        denoise_img(net, source, target)
