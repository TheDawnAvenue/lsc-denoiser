import conv_filter
import auto_filter
import autoencoder_28
import autoencoder_56
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets, transforms
import numpy as np
import os
import csv
from torchvision.utils import save_image
from skimage.util import random_noise


NUM_EPOCHS = 1000
LEARNING_RATE = 1e-3

TRAINING_DIR = "./Dataset/Train"
VALIDATION_DIR = "./Dataset/Validation"
LOG_DIR = "./Logs/" + conv_filter.model_name()
MODEL_DIR = "./Models/" + conv_filter.model_name()
OUTPUT_IMAGE_DIR = "./OutputImage/" + conv_filter.model_name()


def get_device():
    if torch.cuda.is_available():
        device = 'cuda:0'
    else:
        device = 'cpu'
    return device


def make_dirs():
    dirs = [
        TRAINING_DIR,
        VALIDATION_DIR,
        LOG_DIR,
        MODEL_DIR,
        OUTPUT_IMAGE_DIR,
    ]

    for dir_name in dirs:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)


def get_dataloader(data_dir):
    transform = transforms.Compose([transforms.RandomCrop(236), transforms.ToTensor()])
    dataset = datasets.ImageFolder(data_dir, transform=transform)

    return torch.utils.data.DataLoader(dataset, batch_size=conv_filter.batch_size(), shuffle=True)


def write_logs(filename, train_losses, valid_losses):
    with open(filename, 'w', newline='') as csv_file:
        wr = csv.writer(csv_file, quoting=csv.QUOTE_ALL)
        wr.writerow(["epoch", "train_loss", "valid_loss"])
        for epoch, row in enumerate(zip(train_losses, valid_losses)):
            wr.writerow([epoch + 1, row[0], row[1]])


def train():
    device = get_device()
    net = conv_filter.DenoiserModel().float()
    net = net.to(device)

    criterion = nn.MSELoss()
    optimizer = optim.Adam(net.parameters(), lr=LEARNING_RATE)

    dataloader_train = get_dataloader(TRAINING_DIR)
    dataloader_valid = get_dataloader(VALIDATION_DIR)

    best_loss = 100
    best_epoch = 0
    train_losses = []
    valid_losses = []

    for epoch in range(NUM_EPOCHS):

        net.train()
        running_loss = 0.0

        for data in dataloader_train:
            img, _ = data  # we do not need the image labels
            # add noise to the image data
            noisy_img = torch.tensor(random_noise(img, mode='speckle', mean=0, var=0.1, clip=True), dtype=torch.float)
            # clip to make the values fall between 0 and 1
            noisy_img = np.clip(noisy_img, 0., 1.)
            noisy_img = noisy_img.to(device)

            optimizer.zero_grad()
            outputs = net(noisy_img.float())
            loss = criterion(outputs, img.to(device))
            loss.backward()
            optimizer.step()

            running_loss += loss.item()

        train_loss = running_loss / len(dataloader_train)
        train_losses.append(train_loss)
        print("+ Epoch Training finished")

        net.eval()
        running_loss = 0.0

        with torch.no_grad():
            for data in dataloader_valid:
                img, _ = data  # we do not need the image labels
                # add noise to the image data
                noisy_img = torch.tensor(random_noise(img, mode='speckle', mean=0, var=0.1, clip=True),
                                         dtype=torch.float)
                # clip to make the values fall between 0 and 1
                noisy_img = np.clip(noisy_img, 0., 1.)
                noisy_img = noisy_img.to(device)

                outputs = net(noisy_img.float())
                loss = criterion(outputs, img.to(device))

                running_loss += loss.item()

        valid_loss = running_loss / len(dataloader_valid)
        valid_losses.append(valid_loss)
        print("+ Epoch Validation finished")

        if valid_loss < best_loss:
            best_loss = valid_loss
            best_epoch = epoch
            torch.save(net.state_dict(), MODEL_DIR + "./best_net.pth")
            print("# New best epoch: {}".format(epoch + 1))

        print('# Epoch {} of {}, Train Loss: {:.5f}, Valid Loss: {:.5f}'.format(
            epoch + 1, NUM_EPOCHS, train_loss, valid_loss))
        save_image(noisy_img.cpu().data, OUTPUT_IMAGE_DIR + '/{}_noisy.png'.format(epoch + 1))
        save_image(outputs.cpu().data, OUTPUT_IMAGE_DIR +'/{}_denoised.png'.format(epoch + 1))

    write_logs(LOG_DIR + "/training.csv", train_losses, valid_losses)
    print("Training finished!")
    print("Best epoch was {} with loss {:.5f}".format(best_epoch + 1, best_loss))


if __name__ == '__main__':
    print("Start training on {}".format(get_device()))
    make_dirs()
    train()
