import torch
import torch.nn as nn
import torch.nn.functional as F


def model_name():
    return "ConvFilter"


def batch_size():
    return 16


class DenoiserModel(nn.Module):
    def __init__(self):
        super(DenoiserModel, self).__init__()
        # encoder layers
        self.enc1 = nn.Conv2d(3, 224, kernel_size=3, padding=1)
        self.enc2 = nn.Conv2d(224, 224, kernel_size=3, padding=1)
        self.enc3 = nn.Conv2d(224, 224, kernel_size=3, padding=1)

        self.out = nn.Conv2d(224, 3, kernel_size=3, padding=1)

    def forward(self, x):
        # encode
        x = F.relu(self.enc1(x))
        x = F.relu(self.enc2(x))
        x = F.relu(self.enc3(x))

        x = torch.sigmoid(self.out(x))
        return x
