import torch
import torch.nn as nn
import torch.nn.functional as F


def model_name():
    return "Autoencoder28"


def batch_size():
    return 16


class DenoiserModel(nn.Module):
    def __init__(self):
        super(DenoiserModel, self).__init__()
        # encoder layers
        self.enc1 = nn.Conv2d(3, 224, kernel_size=3, padding=1)
        self.enc2 = nn.Conv2d(224, 112, kernel_size=3, padding=1)
        self.enc3 = nn.Conv2d(112, 56, kernel_size=3, padding=1)
        self.enc4 = nn.Conv2d(56, 28, kernel_size=3, padding=1)
        self.pool = nn.MaxPool2d(2, 2)

        # decoder layers
        self.dec1 = nn.ConvTranspose2d(28, 28, kernel_size=3, stride=2)
        self.dec2 = nn.ConvTranspose2d(28, 56, kernel_size=3, stride=2)
        self.dec3 = nn.ConvTranspose2d(56, 112, kernel_size=2, stride=2)
        self.dec4 = nn.ConvTranspose2d(112, 224, kernel_size=2, stride=2)
        self.out = nn.Conv2d(224, 3, kernel_size=3, padding=1)

    def forward(self, x):
        # encode
        x = F.relu(self.enc1(x))
        x = self.pool(x)
        x = F.relu(self.enc2(x))
        x = self.pool(x)
        x = F.relu(self.enc3(x))
        x = self.pool(x)
        x = F.relu(self.enc4(x))
        x = self.pool(x)  # the latent space representation

        # decode
        x = F.relu(self.dec1(x))
        x = F.relu(self.dec2(x))
        x = F.relu(self.dec3(x))
        x = F.relu(self.dec4(x))
        x = torch.sigmoid(self.out(x))
        return x
