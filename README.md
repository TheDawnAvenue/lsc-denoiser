# LSC-Denoiser

Abgaberepository für das Denoiserprojekt im Modul Learning and Softcomputing an der FH Wedel.

## Dateien und Verzeichnisse

* `Dataset/` - Hier müssen die Trainings- und Validierungsbilder abgelegt werden
* `Dataset/Train/` - Hier müssen *in einem Unterordner* die Trainingsbilder abgelegt werden
* `Dataset/Validation/` - Hier müssen *in einem Unterordner* die Validierungsbilder abgelegt werden
* `Logs/` - Logs, die während des Trainings geschrieben wurden und den Trainingsverlauf beschreiben
* `Models/` - Sammlung aller fertig trainierten Modelle
* `auto_filter.py` - PyTorch Modellklasse für den AutoFilter
* `autoencoder_28.py` - PyTorch Modellklasse für den Autoencoder28
* `autoencoder_56.py` - PyTorch Modellklasse für den Autoencoder56
* `conv_filter.py` - PyTorch Modellklasse für den ConvFilter
* `denoise.py` - Skript, um ein fertiges Modell zu benutzen um ein Bild zu entrauschen
* `denoise_all.ps1` - Powershell Skript um alle Bilder in einem Ordner mit einem Modell zu entrauschen
* `denoiser_bericht.pdf` - **Projektbericht**
* `noise.py` - Skript zum Verrauschen aller Bilder eines Ordners
* `requirements.txt` - Erforderliche Libraries für das Projekt
* `train.py` - Skript zum Trainieren eines Modells mit den Bildern aus dem `Dataset`-Ordner

## Installation

Um dieses Projekt verwenden zu können, müssen zuerst die verwendeten Bibliotheken installiert werden. Dazu kann aus
dem Projektverzeichnis folgender Befehl verwendet werden:
```bash
pip install -r requirements.txt
```

## Training

Standardmäßig ist das Training für das ConvFilter-Modell eingestellt. Um das Training für ein anderes Modell
durchzuführen, müssen in der `train.py` alle Vorkommen von `conv_filter` durch das gewünschte Modell ersetzt
werden, also entweder `auto_filter`, `autoencoder_28` oder `autoencoder_56`. Im Hauptverzeichnis ist ein Ordner
namens `Dataset` mit den Unterordnern `Train` und `Validation` notwendig. Diese Unterordner benötigen einen
weiteren Unterordner, z.B. den Spielnamen.
Anschließend wird das Skript ohne Parameter ausgeführt:

```bash
python train.py
```

## Manuelles Verrauschen von Bildern

Um das für das Projekt verwendete Rauschen zu erzeugen, kann das `noise.py` Skript verwendet werden.
Dafür müssen die Ausgangsbilder in einem **Unterordner** des dem Skript übergebenen Ordners liegen.

```bash
python noise.py INPUT_DIR OUTPUT_DIR
```
* `INPUT_DIR` ist der Quellordner **In diesem Ordner dürfen nur Bilddateien liegen**
* `OUTPUT_DIR` ist der Zielordner

## Anwendung der Modelle

Um ein Modell außerhalb des Trainings auf ein Bild anzuwenden, kann das `denoise.py` Skript verwendet werden.
Wichtig ist, dass in der Skript-Datei selbst die richtige Modellklasse importiert wird. Dazu muss die Funktion
`load_net` angepasst werden:

```python
def load_net(net_path):
    net = conv_filter.DenoiserModel().float()
    ...
```
In der Zuweisung muss das `conv_filter` durch das gewünschte Modell ersetzt werden.

Danach kann das Skript aufgerufen werden:
```bash
python denoise.py MODEL_PATH INPUT_IMAGE OUTPUT_IMAGE
```
* `MODEL_PATH` ist der Pfad zu einer `.pth`-Datei
* `INPUT_IMAGE` ist der Pfad zu einer Bilddatei
* `OUTPUT_IMAGE` ist der Pfad für die Ausgabedatei

Um den Prozess für viele Bilder zu vereinfach kann das `denoise_all.ps1`-Powershell-Skript verwendet werden. 
Es entrauscht automatisch einen ganzen Ordner gefüllt mit Bildern. Dazu nutzt es intern das `denoise.py`-Skript.

```bash
powershell ./denoise_all.ps1 INPUT_DIR MODEL_PATH OUTPUT_DIR
```
* `INPUT_DIR` ist der Quellordner **In diesem Ordner dürfen nur Bilddateien liegen**
* `MODEL_PATH` ist der Pfad zu einer `.pth`-Datei
* `OUTPUT_DIR` ist der Zielordner **Dieser Ordner muss existieren**
