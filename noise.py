import sys

import torch
from torchvision import datasets, transforms
import os
from torchvision.utils import save_image
from skimage.util import random_noise


def get_dataloader(data_dir):
    transform = transforms.Compose([transforms.ToTensor()])
    dataset = datasets.ImageFolder(data_dir, transform=transform)

    return torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=True)


def make_noisy(input_dir, output_dir):
    dataloader_train = get_dataloader(input_dir)
    i = 0

    for data in dataloader_train:
        img, _ = data  # we do not need the image labels
        # add noise to the image data
        noisy_img = torch.tensor(random_noise(img, mode='speckle', mean=0, var=0.1, clip=True), dtype=torch.float)
        save_image(noisy_img.data, output_dir + "/noisy_{:03d}.png".format(i))
        i += 1


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Unexpected argument list! Expected 2 arguments.")

    if not os.path.exists(sys.argv[2]):
        os.makedirs(sys.argv[2])

    make_noisy(sys.argv[1], sys.argv[2])
